/*
 * PixelFlut32.h
 *
 *  Created on: 07.09.2019
 *      Author: ledlady
 */

#ifndef PIXELFLUT32_H_
#define PIXELFLUT32_H_

#include "Arduino.h"

#define XYTOCANVAS(x, y) x + y * WIDTH

struct pixelflutRGB_str {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct pixelflutWH_str {
	uint16_t width;
	uint16_t height;
};

template <uint16_t WIDTH, uint16_t HEIGHT>
class PixelFlut32 {
public:
	// raw pixel canvas use at your own risk - i just could not be bothered to use get/set...
	pixelflutRGB_str canvas[WIDTH * HEIGHT];
	// Result of the last run operation in string form
	char RESPONSE_STR[128];
	// If you change any of this strings, change the length in the strncpy below
	const char* NO_CMD_STR = "This command is not recognized";
	const char* HELP_STR = "This is the ESP32 Pixelflut-Server.";

	PixelFlut32() {
		memset(RESPONSE_STR, 0x00, 128);
	}
	virtual ~PixelFlut32() { }

	void setPixel(uint32_t x, uint32_t y, pixelflutRGB_str rgb) {
		canvas[XYTOCANVAS(x, y)] = rgb;
	}

	pixelflutRGB_str getPixel(uint32_t x, uint32_t y) {
		return canvas[XYTOCANVAS(x, y)];
	}

	pixelflutWH_str getSize() {
		return {WIDTH, HEIGHT};
	}

	void parseAndRun(char* input) {
		char* cmd = strtok(input, " ");

		if (cmd[0] == 'P' && cmd[1] == 'X') { // PX
			const char delimiter[] = {' ', '\r'};
			int x = atoi(strtok(NULL, delimiter));
			int y = atoi(strtok(NULL, delimiter));
			char* ac = strtok(NULL, " ");
			if (ac) { // Set pixel command
				setPixel(x, y, atorgb(ac));
				memset(RESPONSE_STR, 0x00, 128);
				// pixelflutRGB_str rgb = atorgb(ac);
			} else {  // Get pixel command
				char rgbstr[6];
				rgbtoa(rgbstr, getPixel(x, y));
				snprintf(RESPONSE_STR, 128, "PX %u %u %s", x, y, rgbstr);
			}
		} else if (cmd[0] == 'S' && cmd[1] == 'I' && cmd[2] == 'Z' && cmd[3] == 'E') { // SIZE
			snprintf(RESPONSE_STR, 128, "SIZE %u %u", WIDTH, HEIGHT);
		} else if (cmd[0] == 'H' && cmd[1] == 'E' && cmd[2] == 'L' && cmd[3] == 'P') { // HELP
			strncpy(RESPONSE_STR, HELP_STR, 36);
		} else {
			strncpy(RESPONSE_STR, NO_CMD_STR, 31);
		}

	}
private:

	pixelflutRGB_str atorgb(char* p) {
		char padded[] = {p[0], p[1], ' ', p[2], p[3], ' ', p[4], p[5]};
		char* rem;
		return {(uint8_t) strtol(padded, &rem, 16),
				(uint8_t) strtol(rem, &rem, 16),
			    (uint8_t) strtol(rem, nullptr, 16)};
	}

	void rgbtoa(char* output, pixelflutRGB_str color) {
		snprintf(output, 6, "%02X%02X%02X", color.r, color.g, color.b);
	}

};


#endif /* PIXELFLUT32_H_ */

#include "Arduino.h"
#include <WiFi.h>
#include <NeoPixelBus.h>
#include <vector>
#include "PixelFlut32.h"

using namespace std;

#define CONFIG_WIFI_NETWORK_SSID "WiFi"
#define CONFIG_WIFI_NETWORK_PASS "123456"
#define CLIENT_INPUT_LEN 256

struct PixelFlutClient {
	WiFiClient* client;
	char input[CLIENT_INPUT_LEN];
	uint32_t inputOffset;
	PixelFlutClient(WiFiClient* c) : client(c), input(""), inputOffset(0) {
		memset(input, 0x00, CLIENT_INPUT_LEN);
	}
	~PixelFlutClient() {
		if (client) {
			client->stop();
			delete client;
		}
		memset(input, 0x00, CLIENT_INPUT_LEN);
	}
};

PixelFlut32<16, 8> flut;
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(128, 4);
WiFiServer flutServer(1337);
std::vector<PixelFlutClient*> clients;
uint32_t nextRenderTime = 0;

void setup() {
    Serial.begin(115200);
    while (!Serial); // wait for serial attach

	// Connect to WiFi
	Serial.print("Connecting WiFI STA");
	WiFi.begin(CONFIG_WIFI_NETWORK_SSID,  CONFIG_WIFI_NETWORK_PASS);
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("Done");

	// Bring up pixelflut server, led strip and button
	flutServer.begin();
    strip.Begin();
    strip.Show();
    pinMode(18, INPUT);

}

void loop() {

	// Add client to queue
	if (flutServer.hasClient()) {
		Serial.println("Welcome.");
		clients.push_back(new PixelFlutClient(new WiFiClient(flutServer.available())));
	}

	// Remove inactive clients
	auto end = std::remove_if(clients.begin(), clients.end(), [](PixelFlutClient* client) {
		 // dont do this kids, use a simple predicates and make the input const, i cant just be bothered to do it properly
		if (!client || !client->client || !client->client->connected()) {
			if (client) delete client;
			Serial.println("Cya.");
			return true;
		} else {
			return false;
		}
	});
	clients.erase(end, clients.end());

	// Receive data from clients and process if the buffer is full or a line end occures
	try {
		for (auto clientit = clients.begin(); clientit != clients.end(); clientit++) {
			PixelFlutClient* client = *clientit;
			if (client && client->client && client->client->connected() && client->client->available()) {
				bool fullLineFlag = false;

				// receive data into clients input buffer
				while (client->client->available()) {
					char in = client->client->read();

					// We use linux line endings here, ignore line carriage return
					if (in == 0x0C) continue;

					// Read char from input
					client->input[client->inputOffset++] = in;

					// trigger on line feed return
					if (in == '\n') {
						fullLineFlag = true;
						client->input[client->inputOffset] = 0x00;
						break;
					}
				}

				// Test if the input buffer is full or the full line flag is set
				if (fullLineFlag || client->inputOffset == CLIENT_INPUT_LEN - 1) {
					// send data to pixelflut
					flut.parseAndRun(client->input);
					client->client->write(flut.RESPONSE_STR);

					// reset input buffer
					memset(client->input, 0x00, CLIENT_INPUT_LEN);
					client->inputOffset = 0;
				}

			}
		}

		// Write pixel data to leds (every 20ms = 50fps)
		if (millis() > nextRenderTime) {
			nextRenderTime = millis() + 20;
			for (uint32_t pidx = 0; pidx < 128; pidx++) {
				pixelflutRGB_str rgb = flut.canvas[pidx];
				strip.SetPixelColor(pidx, {rgb.r, rgb.g, rgb.b});
			}
			strip.Show();
		}

		// If the right button is down clear the flut buffer
		if (digitalRead(18) == LOW) {
			for (uint32_t pidx = 0; pidx < 128; pidx++) {
				flut.canvas[pidx] = {0, 0, 0};
			}
		}

	} catch (...) { Serial.println("Exception"); }

}
